pytagsfs (0.9.2-5) unstable; urgency=low

  * [7a307cd] Switch architecture to all

 -- Ritesh Raj Sarraf <rrs@debian.org>  Mon, 29 Aug 2011 13:26:36 +0530

pytagsfs (0.9.2-4) unstable; urgency=low

  * [270afda] Drop fuse-utils from dependency and add fuse to Recommends.
    Thanks to Robert Millan (Closes: 634329)
  * [4f8cfe2] add build-indep and arch targets
  * [4465dfc] Drop Build-Depends-Indep
  * [ea25dee] Switch to dh_python2
  * [ca1053a] fix spelling error in changelog
  * [f7fc4d5] update standards version to 3.9.2
  * [fed3007] Add fuse | fuse4bsd to Recommends

 -- Ritesh Raj Sarraf <rrs@debian.org>  Fri, 26 Aug 2011 14:26:05 +0530

pytagsfs (0.9.2-3) unstable; urgency=low

  * Team upload.

  [ Ritesh Raj Sarraf ]
  * Change address to my official Debian address
  * Add debian/source/format to explicitly specify the source format

  [ Jakub Wilk ]
  * Pass --executable=/usr/bin/python to setup.py, so that no versioned Python
    shebangs are used (closes: #595410).

 -- Jakub Wilk <jwilk@debian.org>  Sun, 01 May 2011 12:52:43 +0200

pytagsfs (0.9.2-2) unstable; urgency=low

  * Fix wrong installation of files in /usr/local
    (Closes: #569495) 
  * Bump Standards Version to 3.8.4 (No changes required) 

 -- Ritesh Raj Sarraf <rrs@researchut.com>  Fri, 12 Feb 2010 14:36:32 +0530

pytagsfs (0.9.2-1) unstable; urgency=low

  * New Upstream Release 
  * debian/control
    + Replace python-all-dev with python-all
    + Move python-all to Build-Depends since we need it during clean
    + Bump python-all version dependency to > 2.5
    + Add misc:Depends to Dependency
  * Run some testcases during build because upstream recommends it
    (Add multiple build deps for the testcases)

 -- Ritesh Raj Sarraf <rrs@researchut.com>  Mon, 18 Jan 2010 17:34:18 +0530

pytagsfs (0.9.1-1) unstable; urgency=low

  * New Upstream Release
  * Remove Y Giridhar Appaji Nag <appaji@debian.org> from Uploaders
  * Remove dependency on python-pyinotify and add new dependency on
    python-inotifyx
  * Update Standards-Version to 3.8.3 (No changes required)
  * Add a get-orig-source target and update README.source
  * Add Build Dependency for python-all-dev to support all python versions
  * Switch patch management to quilt
    + Drop 02_pyinotify_compatibility patch since we are now moving to
      python-inotifyx

 -- Ritesh Raj Sarraf <rrs@researchut.com>  Fri, 30 Oct 2009 15:42:10 +0530

pytagsfs (0.9.0-2) unstable; urgency=low

  * Take maintenance from Y Giridhar Appaji Nag
  * Pull patch 02_pyinotify_compatibility.dpatch from upstream
    to fix compatibility issues with newer, backwards incompatible, pyinotify
  * Bump Standards Version to 3.8.1 (No changes required) 

 -- Ritesh Raj Sarraf <rrs@researchut.com>  Sat, 16 May 2009 00:47:09 +0530

pytagsfs (0.9.0-1) unstable; urgency=low

  * New upstream release

 -- Y Giridhar Appaji Nag <appaji@debian.org>  Sat, 07 Mar 2009 11:14:59 +0530

pytagsfs (0.9.0~rc1-1) unstable; urgency=low

  * New upstream release
    + Needs python-fuse to build add to Build-Depends

 -- Y Giridhar Appaji Nag <appaji@debian.org>  Wed, 28 Jan 2009 22:11:56 +0530

pytagsfs (0.8.0-1) unstable; urgency=low

  [ Sandro Tosi ]
  * debian/control
    - switch Vcs-Browser field to viewsvn

  [ Y Giridhar Appaji Nag ]
  * New upstream release
  * Add README.source to be compliant with policy 3.8.0
  * Update Maintainer: to official Debian ID
  * Add python-libxml2 to Build-Depends-Indep:

 -- Y Giridhar Appaji Nag <appaji@debian.org>  Sat, 22 Nov 2008 23:05:28 +0530

pytagsfs (0.7.1-2) unstable; urgency=low

  * We need fusermount hence depend on fuse-utils

 -- Y Giridhar Appaji Nag <giridhar@appaji.net>  Fri, 15 Aug 2008 16:14:36 +0530

pytagsfs (0.7.1-1) unstable; urgency=low

  * New upstream release
    + Depends on python-fuse >= 0.2 and python-sclapp >= 0.5.2
    + Build-Depends also on python-sclapp >= 0.5.2
  * Remove patch 02_fuse_exceptions merged upstream
  * Update Standards-Version to 3.8.0 (no changes required)

 -- Y Giridhar Appaji Nag <giridhar@appaji.net>  Sun, 03 Aug 2008 16:07:34 +0530

pytagsfs (0.6.0-2) unstable; urgency=low

  * Change Section from python to utils
  * Patch 01_bug_reporting to set the bug reporting location to Debian and not
    upstream.
  * Patch 02_fuse_exceptions to catch any exception and log an error when fuse
    initialisation fails (like the user is not in the fuse group).

 -- Y Giridhar Appaji Nag <giridhar@appaji.net>  Wed, 30 Apr 2008 17:22:28 +0530

pytagsfs (0.6.0-1) unstable; urgency=low

  * Initial release (Closes: #471971)

 -- Y Giridhar Appaji Nag <giridhar@appaji.net>  Sat, 05 Apr 2008 19:42:17 +0530
