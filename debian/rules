#!/usr/bin/make -f

include /usr/share/quilt/quilt.make

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

PYVERS=$(shell pyversions -vr)

PKG_NAME=pytagsfs
PKG_VER=$(shell dpkg-parsechangelog | sed -rne 's,^Version: ([^-]+).*,\1,p')

get-orig-source:
	uscan --force-download --rename --download-version=$(PKG_VER) --destdir=.

build: build-arch build-indep

build-arch: patch build-stamp
build-indep: patch build-stamp

build-stamp: $(QUILT_STAMPFN) $(PYVERS:%=build-python%)
	touch $@

build-python%:
	dh_testdir

	# Currently failing tests that have been disabled
	# fs, blackbox, sourcetreerep, sourcetreemon
	python$* setup.py test --tests tests.common,tests.manager,tests.mutagen_meta_store,tests.optgroup,tests.pathstore,tests.pytagsfs_meta_store,tests.pytypes,tests.regex,tests.sourcetree,tests.subspat,tests.test_lines_meta_store,tests.util,tests.values

	python$* setup.py build --executable=/usr/bin/python
	touch $@

clean: unpatch
	dh_testdir
	dh_testroot

	python setup.py clean --all
	rm -f build-stamp
	rm -rf build
	find . -name '*.py[co]' | xargs rm -f
	rm -f modules/pytagsfs/version.py
	dh_clean

install: $(PYVERS:%=install-python%)
	touch $@
	
install-python%: build
	dh_testdir
	dh_testroot
	dh_installdirs
	python$* setup.py install --root=$(CURDIR)/debian/pytagsfs --prefix=/usr

# Build architecture-independent files here.
binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs NEWS
	dh_installdocs
	dh_python2
	dh_compress -X.py
	dh_fixperms
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb

# Build architecture-dependent files here.
binary-arch: build install
# We have nothing to do by default.

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install patch unpatch get-orig-source
