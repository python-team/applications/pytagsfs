# Copyright (c) 2007-2008 Forest Bond.
# This file is part of the pytagsfs software package.
#
# pytagsfs is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 2 as published by the Free
# Software Foundation.
#
# A copy of the license has been included in the COPYING file.

# Import locale from sclapp before all other imports to work around bug in
# stdlib.
# See http://www.selenic.com/mercurial/wiki/index.cgi/Character_Encoding_On_OSX.
from sclapp import locale

import sys, os

from optparse import OptionParser, OptionValueError

import sclapp
from sclapp import CriticalError

from pytagsfs.subspat import SubstitutionPattern, Error as PatternError
from pytagsfs.metastore.mutagen_ import MutagenFileMetaStore
from pytagsfs import __version__ as version


def print_tags(filenames):
    failed_filenames = []
    for filename in filenames:
        print filename

        try:
            f = MutagenFileMetaStore.tags_class(filename)
            if f is None:
                raise IOError('Invalid file: %s' % filename)
        except Exception, e:
            sclapp.printCritical(unicode(e))
            failed_filenames.append(filename)
            print
            continue

        print f.pprint()
        print

    if failed_filenames:
        return 1
    return 0


def apply_substitution_pattern(tags, substitution_pattern):
    if substitution_pattern:
        current_values = MutagenFileMetaStore.extract(tags)
        splitter = substitution_pattern.get_splitter(current_values)
        pattern_tags = substitution_pattern.split(filename)
    else:
        pattern_tags = {}

    MutagenFileMetaStore.inject(tags, pattern_tags)


def apply_operations(tags, operations):
    for opname, arg in operations:
        if opname == 'add':
            name, value = arg.split('=')
            tags[name] = tags.get(name, []) + [value]
        elif opname == 'set':
            name, value = arg.split('=')
            tags[name] = [value]
        elif opname == 'remove':
            try:
                del tags[arg]
            except KeyError:
                sclapp.printWarning(u'No such tag: %s' % arg)


def update_tags(format, operations, filenames):
    substitution_pattern = None
    if format:
        substitution_pattern = SubstitutionPattern(format)

    failed_filenames = []

    for filename in filenames:
        print filename

        try:
            tags = MutagenFileMetaStore.tags_class(filename)
            if tags is None:
                raise IOError('Invalid file: %s' % filename)
        except (OSError, IOError), e:
            sclapp.printCritical(unicode(e))
            failed_filenames.append(filename)
            continue

        try:
            apply_substitution_pattern(tags, substitution_pattern)
        except PatternError, e:
            sclapp.printCritical(unicode(e))
            failed_filenames.append(filename)
            continue

        apply_operations(tags, operations)

        tags.save()

    if failed_filenames:
        sclapp.printCritical('Failed to process the following files:')
    for filename in failed_filenames:
        sclapp.printCritical(filename)

    if failed_filenames:
        return 1
    return 0


def parse_set_values(set_exprs):
    set_values = {}
    for expr in set_exprs:
        try:
            k, v = expr.split('=')
        except (TypeError, ValueError):
            raise CriticalError(1, 'Invalid set expression: %s' % expr)
        set_values[k] = v
    return set_values


class PyTagsOptionParser(OptionParser):
    def __init__(self):
        OptionParser.__init__(
          self,
          usage = '%prog [options] {file} [file...]',
          version = '%%prog version %s' % version,
        )

        self.defaults['operations'] = []

        self.add_option(
          '--format',
          action = 'callback',
          type = 'str',
          callback = self.cb_set_format,
          help = (
            'tag files using meta-data parsed from filenames according to PATTERN; '
            'see the manual page for more information.'
          ),
        )
        self.add_option(
          '--set',
          action = 'callback',
          type = 'str',
          callback = self.cb_append_operation,
          metavar = 'FOO=BAR',
          help = 'set tag named FOO to value BAR',
        )
        self.add_option(
          '--remove',
          action = 'callback',
          type = 'str',
          callback = self.cb_append_operation,
          metavar = 'FOO',
          help = 'unset tag named FOO',
        )
        self.add_option(
          '--add',
          action = 'callback',
          type = 'str',
          callback = self.cb_append_operation,
          metavar = 'FOO=BAR',
          help = 'add value BAR as new value for tag FOO',
        )

    def cb_set_format(self, option, opt_str, value, parser):
        if parser.values.operations:
            raise OptionValueError("--format must be specified first")
        setattr(parser.values, option.dest, value)

    def cb_append_operation(self, option, opt_str, value, parser):
        parser.values.operations.append((opt_str.lstrip('-'), value))


@sclapp.main_function
def main(argv):
    parser = PyTagsOptionParser()
    values, args = parser.parse_args(argv[1:])

    if len(args) < 1:
        raise CriticalError(1, 'Too few arguments.')

    if not (values.format or values.operations):
        return print_tags(args)
    else:
        return update_tags(values.format, values.operations, args)
